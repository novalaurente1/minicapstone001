let potMoney = 2000000000;
let items = [
  {
    name: "Starbucks Latte",
    image: "assets/images/starbucks.jpg",
    price: 170,
    quantity: 0
  },
  {
    name: "Modernized Jeepney",
    image: "assets/images/jeepney.jpg",
    price: 2000000,
    quantity: 0
  },
  {
    name: "Toyota Fortuner",
    image: "assets/images/fortuner.jpg",
    price: 2275000,
    quantity: 0
  },
  {
    name: "Patrol Boat",
    image: "assets/images/boat.jpg",
    price: 20000000,
    quantity: 0
  },
  {
    name: "Trains",
    image: "assets/images/train.jpg",
    price: 200000000,
    quantity: 0
  },
  {
    name: "Secondhand Yellow Porsche",
    image: "assets/images/porsche.jpg",
    price: 17000000,
    quantity: 0
  },
  {
    name: "Stan Smith",
    image: "assets/images/stan-smith.jpg",
    price: 4800,
    quantity: 0
  },
  {
    name: "iPhone 11 256GB",
    image: "assets/images/iphone.jpg",
    price: 56990,
    quantity: 0
  },
  {
    name: "Teachers Salary",
    image: "assets/images/teacher.jpg",
    price: 40000,
    quantity: 0
  },
  {
    name: "Townhouse",
    image: "assets/images/townhouse.jpg",
    price: 8000000,
    quantity: 0
  },
  {
    name: "PBA Team",
    image: "assets/images/pba-team.jpg",
    price: 120000000,
    quantity: 0
  },
  {
    name: "7-11 Franchise",
    image: "assets/images/seven-eleven.jpg",
    price: 1500000,
    quantity: 0
  }
];

const addToCart = itemIndex => {
  items.forEach((item, index) => {
    if (parseInt(itemIndex) === index) {
      items[index].quantity++;
    }
  });
};

const removeFromCart = itemIndex => {
  items.forEach((item, index) => {
    if (parseInt(itemIndex) === index && item.quantity > 0) {
      items[index].quantity--;
    }
  });
};

const computeTotal = () => {
  let totalPurchase = 0;

  items.forEach(item => {
    totalPurchase += item.quantity * item.price;
  });

  return potMoney - totalPurchase;
};

const renderTotal = () => {
  let finalComputation = computeTotal().toLocaleString();

  if (parseFloat(finalComputation) <= 0) {
    finalComputation = 0;
  }

  if (parseFloat(finalComputation) == 0) {
    setTimeout(() => {
      alert("Congratulations! You just spent 2B pesos. How do you feel?");
    }, 100);
  }

  document.getElementById("total").innerHTML = finalComputation;

};

(() => {
  let shoppingListElem = document.getElementById("shopping-list");

  items.forEach((item, index) => {
    // create a card element
    let cardElem = document.createElement("div");
    cardElem.classList.add("card");
    cardElem.classList.add("col-lg-3");
    cardElem.style.padding = "0";
    //

    // create a card image
    let cardImage = document.createElement("img");
    cardImage.setAttribute("src", item.image);
    cardImage.style.alignSelf = "center";
    cardImage.style.width = "100%";
    cardImage.style.minHeight = "190px";

    // create a card body
    let cardBody = document.createElement("div");
    cardBody.classList.add("card-body");

    // create a card title
    let cardTitle = document.createElement("h6");
    cardTitle.classList.add("card-title");
    cardTitle.innerHTML = item.name;

    // create a card text
    let cardText = document.createElement("p");
    cardText.classList.add("card-text");
    cardText.innerHTML = item.price.toLocaleString();

    // div for buttons
    let buttonHolder = document.createElement("div");
    buttonHolder.style.display = 'flex';
    buttonHolder.style.justifyContent = 'center';
    
    // create the input field
    let inputField = document.createElement("input");
    inputField.type = "text";
    inputField.id = "input-" + index;
    inputField.value = item.quantity;
    inputField.style.width = "50px";

    // create a plus button
    let plusButton = document.createElement("button");
    plusButton.innerHTML = "+";
    plusButton.style.backgroundColor = "#43ac6a";
    plusButton.setAttribute("data-index", index);
    plusButton.addEventListener("click", e => {
      let elem = e.currentTarget;
      let elemIndex = elem.getAttribute("data-index");

      addToCart(elemIndex);
      document.getElementById("input-" + elemIndex).value++;
      renderTotal();
    });

    // create a minus button
    let minusButton = document.createElement("button");
    minusButton.innerHTML = "-";
    minusButton.style.backgroundColor = "#f04123";
    minusButton.setAttribute("data-index", index);
    minusButton.addEventListener("click", e => {
      let elem = e.currentTarget;
      let elemIndex = elem.getAttribute("data-index");

      removeFromCart(elemIndex);
      document.getElementById("input-" + elemIndex).value--;
      renderTotal();
    });

    // construct the whole card
    cardElem.appendChild(cardImage);
    cardElem.appendChild(cardBody);
    cardBody.appendChild(cardTitle);
    cardBody.appendChild(cardText);
    cardElem.appendChild(buttonHolder);
    buttonHolder.appendChild(minusButton);
    buttonHolder.appendChild(inputField);
    buttonHolder.appendChild(plusButton);
    shoppingListElem.appendChild(cardElem);
  });

  renderTotal();
})();